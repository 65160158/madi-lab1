import type Product from "./Product";

export default interface Cart {
  id: number;
  product: Product;
  num: number;
}
