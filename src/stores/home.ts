import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import type Cart from "@/types/Cart";

export const useHomeStore = defineStore("home", () => {
  const whiskey = ref<Product[]>([
    {
      id: 1,
      image:
        "https://cdn.shopify.com/s/files/1/0066/2791/7914/products/2799.jpg?v=1560159485",
      name: "Johnnie Walker Blue Label (750 ml)",
      subtitle: "whiskey",
      price: 9505.0,
      num: 1,
      amount: 1,
    },
    {
      id: 2,
      image:
        "https://www.comprasparaguai.com.br/media/fotos/modelos/whisky_johnnie_walker_green_label_750ml_114460_550x550.jpg",
      name: "Johnnie Walker Green Label (750 ml)",
      subtitle: "whiskey",
      price: 2159.0,
      num: 1,
      amount: 1,
    },
    {
      id: 3,
      image:
        "https://cdn.shopify.com/s/files/1/0055/5125/9719/files/8_0b521226-f10a-44ef-99b6-69489417e023.jpg?v=1682549781",
      name: "Johnnie Walker Black Label (750 ml)",
      subtitle: "whiskey",
      price: 1300.0,
      num: 1,
      amount: 1,
    },
    {
      id: 4,
      image:
        "https://acbbuy.com/wp-content/uploads/2023/03/whisky-johnnie-walker-red-label-x-750-ml6878.png",
      name: "Johnnie Walker Red Label (700 ml)",
      subtitle: "whiskey",
      price: 679.0,
      num: 1,
      amount: 1,
    },
  ]);
  return { whiskey };
});
