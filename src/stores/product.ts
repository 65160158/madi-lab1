import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";

export const useProductStore = defineStore("product", () => {
  const whiskey = ref<Product[]>([
    {
      id: 1,
      image:
        "https://cdn.shopify.com/s/files/1/0066/2791/7914/products/2799.jpg?v=1560159485",
      name: "Johnnie Walker Blue Label (750 ml)",
      subtitle: "whiskey",
      price: 9505.0,
      num: 1,
      amount: 1,
    },
    {
      id: 2,
      image:
        "https://www.comprasparaguai.com.br/media/fotos/modelos/whisky_johnnie_walker_green_label_750ml_114460_550x550.jpg",
      name: "Johnnie Walker Green Label (750 ml)",
      subtitle: "whiskey",
      price: 2159.0,
      num: 1,
      amount: 1,
    },
    {
      id: 3,
      image:
        "https://cdn.shopify.com/s/files/1/0055/5125/9719/files/8_0b521226-f10a-44ef-99b6-69489417e023.jpg?v=1682549781",
      name: "Johnnie Walker Black Label (750 ml)",
      subtitle: "whiskey",
      price: 1300.0,
      num: 1,
      amount: 1,
    },
    {
      id: 4,
      image:
        "https://acbbuy.com/wp-content/uploads/2023/03/whisky-johnnie-walker-red-label-x-750-ml6878.png",
      name: "Johnnie Walker Red Label (700 ml)",
      subtitle: "whiskey",
      price: 679.0,
      num: 1,
      amount: 1,
    },
    {
      id: 5,
      image: "https://img.thewhiskyexchange.com/900/blend_han8.jpg",
      name: "Hankey Bannister REGENCY (700 ml)",
      subtitle: "whiskey",
      price: 845.0,
      num: 1,
      amount: 1,
    },
    {
      id: 6,
      image: "https://www.เหล้านอก.com/wp-content/uploads/2020/07/Jack-7.jpg",
      name: "Jack Daniel's No.7 (1000 ml)",
      subtitle: "whiskey",
      price: 1150.0,
      num: 1,
      amount: 1,
    },
  ]);
  const mixer = ref<Product[]>([
    {
      id: 1,
      image:
        "https://th.bing.com/th/id/OIP.KR4T-6z_xt3fDHdH7sTgbwHaK2?pid=ImgDet&rs=1",
      name: "Cola",
      subtitle: "Soft drink",
      price: 27.0,
      num: 1,
      amount: 1,
    },
    {
      id: 2,
      image:
        "https://elmercadobangkok.com/wp-content/uploads/2021/11/Schweppes-Manao-Soda.png",
      name: "Schweppes",
      subtitle: "Soft drink",
      price: 15.0,
      num: 1,
      amount: 1,
    },
    {
      id: 3,
      image:
        "https://api.pns.hk/medias/zoom-front-322330.jpg?context=bWFzdGVyfHBuc2hrL2ltYWdlc3w5MTI4MnxpbWFnZS9qcGVnfGg3Yy9oYjQvOTY3NzA5MTkzMDE0Mi9QTlNISy0zMjIzMzAtZnJvbnQuanBnfGQwYTk0MWJkMDkwMTBlN2E3NTNmNWY5NDU2MjQ4ZmQwODMzNDUzM2UxODMyNWYxNGYwNWM5MjdhZjIzMzU3YmM",
      name: "Soda",
      subtitle: "Soft drink",
      price: 10.0,
      num: 1,
      amount: 1,
    },
    {
      id: 4,
      image: "https://pizzahampstead.com/wp-content/uploads/2016/09/28.jpg",
      name: "Orange Juice",
      subtitle: "Soft drink",
      price: 80.0,
      num: 1,
      amount: 1,
    },
    {
      id: 5,
      image:
        "https://shake-that.com/wp-content/uploads/2015/07/Blue-Hawaii-780x874.jpg",
      name: "Blue Hawaii",
      subtitle: "Soft drink",
      price: 150.0,
      num: 1,
      amount: 1,
    },
    {
      id: 6,
      image: "https://www.carabao.co.th/en/images/Carabao-Drinking-Water.png",
      name: "Water",
      subtitle: "Soft drink",
      price: 15.0,
      num: 1,
      amount: 1,
    },
  ]);
  const snacks = ref<Product[]>([
    {
      id: 1,
      image:
        "https://cdn.shopify.com/s/files/1/0553/8765/2286/articles/hikynvl8pjkjqhvpnok6_1000x1000.jpg?v=1619198610",
      name: "French Fries",
      subtitle: "Snacks",
      price: 50.0,
      num: 1,
      amount: 1,
    },
    {
      id: 2,
      image: "https://belchicken.com/wp-content/uploads/2022/01/nuggets-1.png",
      name: "Nuggets",
      subtitle: "Snacks",
      price: 80.0,
      num: 1,
      amount: 1,
    },
    {
      id: 3,
      image:
        "https://www.foodnetwork.com/content/dam/images/food/fullset/2008/12/23/0/FNmag_Movie-Theater-Style_s4x3.jpg",
      name: "Popcorn",
      subtitle: "Snacks",
      price: 40.0,
      num: 1,
      amount: 1,
    },
    {
      id: 4,
      image:
        "https://www.foodservicedirect.com/media/catalog/product/0/0/00028400443593_1_wcl3llapxxx2wf7p.jpg?width=1200&height=1200&quality=85&fit=bounds",
      name: "Chips",
      subtitle: "Snacks",
      price: 25.0,
      num: 1,
      amount: 1,
    },
    {
      id: 5,
      image:
        "https://www.thedailymeal.com/img/gallery/the-ultimate-ranking-of-the-best-fried-chicken-chains/intro-1659368501.jpg",
      name: "Fried Chicken",
      subtitle: "Snacks",
      price: 150.0,
      num: 1,
      amount: 1,
    },
    {
      id: 6,
      image:
        "https://www.quiktrip.com/app/uploads/2021/10/MicrosoftTeams-image-5.jpg",
      name: "Hot Dog",
      subtitle: "Snacks",
      price: 30.0,
      num: 1,
      amount: 1,
    },
  ]);

  return { whiskey, mixer, snacks };
});
