import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useUserStore = defineStore("user", () => {
  const sum = ref(0);
  const amount = ref(0);

  const CL = () => {
    sum.value = sum.value + 27.0;
    items.push({
      name: "Cola",
      price: "฿27.00",
    });
    amount.value = amount.value + 1;
  };

  const SW = () => {
    sum.value = sum.value + 15.0;
    items.push({
      name: "Schweppes",
      price: "฿15.00",
    });
    amount.value = amount.value + 1;
  };

  const SD = () => {
    sum.value = sum.value + 10.0;
    items.push({
      name: "Soda",
      price: "฿10.00",
    });
    amount.value = amount.value + 1;
  };

  const OJ = () => {
    sum.value = sum.value + 80.0;
    items.push({
      name: "Orange Juice",
      price: "฿80.00",
    });
    amount.value = amount.value + 1;
  };

  const BW = () => {
    sum.value = sum.value + 150.0;
    items.push({
      name: "Blue Hawaii",
      price: "฿150.00",
    });
    amount.value = amount.value + 1;
  };

  const WT = () => {
    sum.value = sum.value + 15.0;
    items.push({
      name: "Water",
      price: "฿15.00",
    });
    amount.value = amount.value + 1;
  };

  //delete

  const DJWB = () => {
    items.pop();
    if (amount.value > 0) {
      sum.value = sum.value - 9505.0;
      amount.value = amount.value - 1;
    }
  };

  const DJWG = () => {
    items.pop();
    if (amount.value > 0) {
      sum.value = sum.value - 2159.0;
      amount.value = amount.value - 1;
    }
  };

  const DWBK = () => {
    items.pop();
    if (amount.value > 0) {
      sum.value = sum.value - 1300.0;
      amount.value = amount.value - 1;
    }
  };

  const DJWR = () => {
    items.pop();
    if (amount.value > 0) {
      sum.value = sum.value - 679.0;
      amount.value = amount.value - 1;
    }
  };

  const DREC = () => {
    items.pop();
    if (amount.value > 0) {
      sum.value = sum.value - 845.0;
      amount.value = amount.value - 1;
    }
  };
  const DJDD = () => {
    items.pop();
    if (amount.value > 0) {
      sum.value = sum.value - 1150.0;
      amount.value = amount.value - 1;
    }
  };

  const items: { name: string; price: string }[] = [];

  return {
    CL,
    SW,
    SD,
    OJ,
    BW,
    WT,
  };
});
