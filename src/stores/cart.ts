import { ref, computed, reactive } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import type Cart from "@/types/Cart";
import { useProductStore } from "@/stores/product";

export const useCartStore = defineStore("cart", () => {
  const useProduct = useProductStore();
  let lastId: number = 1;
  const cart = ref<Cart[]>([]);

  function getProduct(subtitle: string) {
    return useProduct.whiskey.filter((item) => item.subtitle === subtitle);
  }
  function addToCart(product: Product) {
    const index = cart.value.findIndex(
      (item) => item.product.id === product.id
    );
    if (index >= 0) {
      cart.value[index].num++;
      return;
    }
    const newCart: Cart = {
      id: lastId++,
      product: JSON.parse(JSON.stringify(product)),
      num: 1,
    };
    cart.value.push(newCart);
  }
  const deleteProduct = (id: number): void => {
    const index = cart.value.findIndex((item) => item.id === id);
    cart.value.splice(index, 1);
  };
  const sumProduct = computed(function () {
    return cart.value.reduce(
      (sum, item) => sum + item.product.price * item.num,
      0
    );
  });
  return {
    getProduct,
    addToCart,
    cart,
    useProduct,
    deleteProduct,
    sumProduct,
  };
});
